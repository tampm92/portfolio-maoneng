import Vue from 'vue';
import * as VueGoogleMaps from 'vue2-google-maps'

Vue.use(VueGoogleMaps, {
    load: {
        key: 'AIzaSyC-2Lr9OrZjRLpECfgXI_0oV11hmSbeIz4',
        libraries: 'places', // This is required if you use the Autocomplete plugin
    },
});