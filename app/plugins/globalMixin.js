import Vue from 'vue'

Vue.mixin({
    data () {
        return {
            translationList: [
                {
                    title: '中文',
                    value: 'en'
                },
                {
                    title: 'ENG',
                    value: 'zh'
                }
            ],
            currentTranslationSelected: 'en',
        }
    },

    mounted () {
        // this.processRoute()
        this.translationProcess()
    },

	methods: {
        processRoute () {
            let queryLangParam = this.$route.query.lang

            let translationValue = this.getCurrentTranslationValue()
            let translationList = this.translationList

            if (queryLangParam !== undefined && queryLangParam !== translationValue) {
                this.setTranslationValue(queryLangParam)
            }

            if (translationValue !== translationList[0].value && queryLangParam === undefined) {
                var route = this.$route

                console.log(route.query, '=====')

                this.$router.push({
                    'name': route.name,
                    'path': route.path,
                    'query': {
                        category: route.query.category,
                        lang: translationValue
                    }
                })
            }
            // if (queryLangParam !== undefined) {
            //     this.setTranslationValue(queryLangParam)
            // }

        },
        reloadPageWithLang (translationValue) {
            var route = this.$route

            console.log(route.query, '=====')

            this.$router.push({
                'name': route.name,
                'path': route.path,
                'query': {
                    category: route.query.category,
                    lang: translationValue
                }
            })
        },
        translationProcess () {
            this.currentTranslationSelected = this.getCurrentTranslationValue()

            this.setTranslateBaseOnLocalStorage()
        },
        setTranslateBaseOnLocalStorage () {
            let translationValue = localStorage.translationValue

            if (translationValue !== undefined) {
                this.setI18nLocale(translationValue)
            } else {
                this.setI18nLocale('en')
            }
        },
        setI18nLocale (translationValue) {
            if (this.$i18n !== undefined) {
                this.$i18n.locale = translationValue
            }
        },
        setTranslationValue (translationValue) {
            localStorage.translationValue = translationValue
        },
        getCurrentTranslationValue () {
            let translationValue = localStorage.translationValue

            if (translationValue === undefined) {
                return this.translationList[0].value
            }

            return translationValue
        },
        setTranslationValueByIndex (index) {
            if (index < this.translationList.length - 1 && index >= 0) {
                this.setTranslationValue(this.translationList[index + 1].value)
            } else if (index === this.translationList.length - 1) {
                this.setTranslationValue(this.translationList[0].value)
            } else {
                this.setTranslationValue(this.translationList[0].value)
            }

            this.reloadPageWithLang(localStorage.translationValue)

            location.reload()
        },
    },
    watch: {
        currentTranslationSelected (newValue, oldValue) {
            // this.setTranslateBaseOnLocalStorage()
        }
    }
})
