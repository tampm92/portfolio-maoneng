const pkg = require('./package')
var siteInfo = require('./content/setup/info.json');
var glob = require('glob');
var path = require('path');

var dynamicRoutes = getDynamicPaths({
  '/news': 'blog/posts/*.json'
});

module.exports = {
  mode: 'universal',
  server: {
    port: 8000,
  },
  /*
  ** Headers of the page
  */
  head: {
    title: 'Maoneng Group',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Developer of Renewable Power Generation Assets' },
      { hid: 'og:title', name: 'og:title', content: 'Maoneng Group | Developer of Renewable Power Generation Assets' },
      { hid: 'og:site_name', name: 'og:site_name', content: 'maoneng.co' },
      { hid: 'og:description', name: 'og:description', content: 'Leading the transition from Fossil fuel to renewable energy' },
      { hid: 'og:image', name: 'og:image', content: 'https://s3-ap-southeast-2.amazonaws.com/maoneng-external-assets/marketing/ogimage.png' }
    ],
    script: [
      // Vendor
      { src: '/theme/vendor/jquery.min.js' },
      { src: '/theme/vendor/jquery.migrate.min.js' },
      { src: '/theme/vendor/bootstrap/js/bootstrap.min.js' },
      { src: '/theme/vendor/owl-carousel/owl.carousel.js' },
      { src: '/theme/vendor/jquery.smooth-scroll.min.js' },
      { src: '/theme/vendor/jquery.back-to-top.min.js' },
      { src: '/theme/vendor/scrollbar/jquery.scrollbar.min.js' },
      { src: '/theme/vendor/magnific-popup/jquery.magnific-popup.min.js' },
      { src: '/theme/vendor/swiper/swiper.jquery.min.js' },
      { src: '/theme/vendor/waypoint.min.js' },
      { src: '/theme/vendor/counterup.min.js' },
      { src: '/theme/vendor/cubeportfolio/js/jquery.cubeportfolio.min.js' },
      { src: '/theme/vendor/jquery.parallax.min.js' },
      { src: '/theme/vendor/jquery.wow.min.js' },

      // General component and settings
      { src: '/theme/js/global.min.js' },
      { src: '/theme/js/components/header-sticky.min.js' },
      { src: '/theme/js/components/scrollbar.min.js' },
      { src: '/theme/vendor/vidbg.min.js' },
      { src: '/theme/js/components/magnific-popup.min.js' },
      { src: '/theme/js/components/swiper.min.js' },
      { src: '/theme/js/components/counter.min.js' },
      { src: '/theme/js/components/portfolio-3-col.min.js' },
      { src: '/theme/js/components/parallax.min.js' },
      { src: '/theme/js/components/wow.min.js' },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: 'https://use.typekit.net/sms3feb.css' },
      { rel: 'stylesheet', href: 'https://use.fontawesome.com/releases/v5.6.3/css/all.css', integrity: 'sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/', crossorigin: 'anonymous' },
      // { rel: 'stylesheet', href: 'https://pro.fontawesome.com/releases/v5.6.3/css/all.css', integrity: 'ha384-LRlmVvLKVApDVGuspQFnRQJjkv0P7/YFrw84YYQtmYG4nK8c+M+NlmYDCv0rKWpG', crossorigin: 'anonymous' },

      { rel: 'stylesheet', href: '/theme/vendor/bootstrap/css/bootstrap.min.css' },
      { rel: 'stylesheet', href: '/theme/vendor/owl-carousel/owl.carousel.min.css' },
      { rel: 'stylesheet', href: '/theme/css/animate.css' },
      { rel: 'stylesheet', href: '/theme/vendor/themify/themify.css' },
      { rel: 'stylesheet', href: '/theme/vendor/scrollbar/scrollbar.min.css' },
      { rel: 'stylesheet', href: '/theme/vendor/magnific-popup/magnific-popup.css' },
      { rel: 'stylesheet', href: '/theme/vendor/swiper/swiper.min.css' },
      { rel: 'stylesheet', href: '/theme/vendor/cubeportfolio/css/cubeportfolio.min.css' },
      { rel: 'stylesheet', href: '/theme/css/style.css' },
      { rel: 'stylesheet', href: '/theme/css/global/global.css' },
    ]
  },

  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },

  /*
  ** Global CSS
  */
  css: [
    '~/assets/style/main.scss'
  ],

  vendor:[
    "vue2-google-maps"
  ],

  generate: {
    routes: dynamicRoutes
  },

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '~/plugins/eventBus.js',
    '~/plugins/i18n.js',
    '~/plugins/vue-moment.js',
    '~/plugins/globalMixin.js',
    { src: "~/plugins/vue2-google-maps", ssr: true },
    // '~/plugins/routeConfig.js'
  ],

  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://github.com/nuxt-community/axios-module#usage
    '@nuxtjs/axios',
    '@nuxtjs/markdownit',
  ],
  markdownit: {
    injected: true,
    preset: 'default',
    breaks: true,
    html: true
  },
  /*
  ** Axios module configuration
  */
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
  },

  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {

    },
    transpile: [/^vue2-google-maps($|\/)/],
  }
}

/**
 * Create an array of URLs from a list of files
 * @param {*} urlFilepathTable
 */
function getDynamicPaths(urlFilepathTable) {
  return [].concat(
    ...Object.keys(urlFilepathTable).map(url => {
      var filepathGlob = urlFilepathTable[url];
      return glob
        .sync(filepathGlob, { cwd: 'content' })
        .map(filepath => `${url}/${path.basename(filepath, '.json')}`);
    })
  );
}
