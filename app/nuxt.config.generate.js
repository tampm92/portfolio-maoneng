// content of nuxt.config.generate.js file
module.exports = {
  ...require('./nuxt.config'),
  ...{
    build: {
      publicPath: 'public/',
    },
  },
}
