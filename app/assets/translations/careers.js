export default {
  en: {
    head: {
      title: 'Maoneng Group | Career',
      meta: [
        { hid: 'description', name: 'description', content: 'Join our passionate team' }
      ]
    },
    button: {
      moreinfo: 'MORE INFO',
      morenews: 'MORE NEWS',
      team: 'MEET THE TEAM'
    },
    heading: {
      title: {
        caps: 'Careers',
        first: 'Join our',
        second: 'passionate',
        third: 'team.'
      }
    },
    jobs: {
      head: {
        title: 'Join our team',
        description: 'We have great career opportunities in different sectors. We are about career growth, collaboration and learning.',
      },
      sydney: [
        {
          title: 'Analyst - Financial Modelling',
          location: 'Sydney',
          jobType: 'Full time',
          idName: 'analystSydney',
          role: 'Your role will include but not limited to running various financial scenarios, analysing and optimising capital structures for utility scale renewable energy projects. You will be working closely with and reporting to the Director for Structured Finance.',
          responsibilityTitle: '',
          responsibilities: [
            ''
          ],
          responsibilitiesDescription: '',
          skillsTitle: '',
          requirements: [
            ''
          ],
          websiteUrl: '#'
        },
        {
          title: 'Solar PV System Design Engineer',
          location: 'Sydney',
          jobType: 'Full time',
          idName: 'engineerSydney',
          role: [
          'Your role will be responsible for all aspects of engineering design, yield analysis and development of new and advanced solar PV systems. You will work closely with our Australian team, key vendors and partners and with local certification and industry bodies.',
          'The individual must be interested and able to work in a multi-national and cultural environment to draw on resources in other locations. ',
          ],
          responsibilityTitle: '',
          responsibilities: [
            ''

          ],
          responsibilitiesDescription: '',
          skillsTitle: '',
          requirements: [
            ''
          ],
          websiteUrl: '#'
        },
      ],
      singapore: [
        {
          title: '',
          location: '',
          jobType: '',
          idName: '',
          role: '',
          responsibilities: [
            ''
          ],
          responsibilitiesDescription: '',
          requirements: [
            ''
          ]
        },
        {
          title: '',
          location: '',
          jobType: '',
          idName: '',
          role: '',
          responsibilities: [
            ''
          ],
          responsibilitiesDescription: '',
          requirements: [
            ''
          ]
        },
      ]
    },
    news: {
      title: {
        first: 'NEWS',
        second: 'Latest News.',
        third: 'Keep up with the latest news.'
      },
    },
  },
  zh: {

  }
}
