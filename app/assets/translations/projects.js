export default {
  en: {
    head: {
      sunraysia: {
        title: 'Maoneng Group Project - Sunraysia Solar Farm',
        meta: [
          { hid: 'description', name: 'description', content: 'Sunraysia Solar Farm is a 255MWDC utility-scale solar photovoltaic (PV) farm near Balranald in southern New South Wales.' }
        ]
      },
      muggaLane: {
        title: 'Maoneng Group Project - Mugga Lane Solar Park',
        meta: [
          { hid: 'description', name: 'description', content: 'Our first major investment in Australia is the iconic 13MW Mugga Lane Solar Park, developed and built under the ACT Government\'s Reverse Solar Auction.' }
        ]
      }
    },
    heading: {
      pageName: 'PROJECT',
    },
    button: {
      visitWebsiteBtn: 'VISIT WEBSITE'
    },
    muggaLane: {
      header: {
        title: {
          first: 'Mugga Lane',
          second: 'Solar Park',
          third: '13MW'
        },
        image: '/images/media_assets/projects/MuggaLane/header-mlsp.jpg'
      },
      statisticOne: {
        caption: '3,000+ homes',
        details: 'supplied with energy'
      },
      statisticTwo: {
        caption: '24,500 MWh',
        details: 'of electricity generated'
      },
      content: [
        'Our first major investment in Australia is the iconic 13MW Mugga Lane Solar Park, developed and built under the ACT Government\'s Reverse Solar Auction.',
        'The solar farm generates approximately 24,500 MWh of electricity, which is enough to power more than 3,000 homes. It is built and maintained by UGL - one of the best solar PV construction companies.'
      ],
      gallery: [
        '/images/media_assets/projects/MuggaLane/Gallery/gallery-mlsp-1.jpg',
        '/images/media_assets/projects/MuggaLane/Gallery/gallery-mlsp-2.jpg',
        '/images/media_assets/projects/MuggaLane/Gallery/gallery-mlsp-3.jpg',
        '/images/media_assets/projects/MuggaLane/Gallery/gallery-mlsp-4.jpg',
        '/images/media_assets/projects/MuggaLane/Gallery/gallery-mlsp-5.jpg'
      ],
      projectMapUrl: '/images/media_assets/projects/MuggaLane/map-mlsp.png',
      websiteUrl: '#',
      mapContent: {
        center: {
          lat: -35.398161,
          lng: 149.149113
        },
        markerPosition: {
          lat: -35.398161,
          lng: 149.149113
        },
        zoom: 8
      }
    },
    sunraysia: {
      header: {
        title: {
          first: 'Sunraysia',
          second: 'Solar Farm',
          third: '255MWDC'
        },
        image: '/images/media_assets/projects/Sunraysia/header-ssf.jpg'
      },
      statisticOne: {
        caption: '50,000+ homes',
        details: 'supplied with energy'
      },
      statisticTwo: {
        caption: '255 MWDC',
        details: 'of electricity generated'
      },
      content: [
        'Sunraysia Solar Farm is a utility-scale solar photovoltaic (PV) farm near Balranald in southern New South Wales.',
        'Sunraysia will have a total capacity of 255MWDC and is scheduled for completion in December 2019.'
      ],
      gallery: [
        '/images/media_assets/projects/Sunraysia/Gallery/gallery-ssf-1.jpg',
      ],
      projectMapUrl: '/images/media_assets/projects/Sunraysia/map-ssf.png',
      websiteUrl: '#',
      mapContent: {
        center: {
          lat: -34.8070837,
          lng: 143.493517
        },
        markerPosition: {
          lat: -34.8070837,
          lng: 143.493517
        },
        zoom: 8
      }
    }
  },
  zh: {
    heading: {
      pageName: 'PROJECTS',
    },
    button: {
      visitWebsiteBtn: '访问网站'
    },
    muggaLane: {
      header: {
        title: {
          first: 'Mugga Lane',
          second: '光伏公园',
          third: '13MW'
        },
        image: '/images/media_assets/projects/MuggaLane/header-mlsp.jpg'
      },
      statisticOne: {
        caption: '所发出的再生能源',
        details: '可供3000多户人家'
      },
      statisticTwo: {
        caption: '每年能产',
        details: '2千4百50多万度电'
      },
      content: [
        '茂能在澳大利亚的第一个标志性项目，是通过澳大利亚首都直辖区政府太阳能逆向拍卖活动开发的标杆性的13MW Mugga Lane太阳能公园。',
        '该太阳能电站大约能产出24,500MWh的电力，足够3,000个家庭使用。该项目由UGL公司承建与维护——该公司是澳大利亚境内最好的一家太阳能光电建设公司之一。'
      ],
      gallery: [
        '/images/media_assets/projects/MuggaLane/Gallery/gallery-mlsp-1.jpg',
        '/images/media_assets/projects/MuggaLane/Gallery/gallery-mlsp-2.jpg',
        '/images/media_assets/projects/MuggaLane/Gallery/gallery-mlsp-3.jpg',
        '/images/media_assets/projects/MuggaLane/Gallery/gallery-mlsp-4.jpg',
        '/images/media_assets/projects/MuggaLane/Gallery/gallery-mlsp-5.jpg'
      ],
      projectMapUrl: '/images/media_assets/projects/MuggaLane/map-mlsp.png',
      websiteUrl: '#',
      mapContent: {
        center: {
          lat: -35.398161,
          lng: 149.149113
        },
        markerPosition: {
          lat: -35.398161,
          lng: 149.149113
        },
        zoom: 8
      }
    },
    sunraysia: {
      header: {
        title: {
          first: 'Sunraysia',
          second: '光伏农场',
          third: '255MWDC'
        },
        image: '/images/media_assets/projects/Sunraysia/header-ssf.jpg'
      },
      statisticOne: {
        caption: '50,000 homes',
        details: 'supplied with energy'
      },
      statisticTwo: {
        caption: '255 MWDC',
        details: 'of electricity generated'
      },
      content: [
        'Sunraysia 光伏农场是茂能集团位于新南威尔士州南部Balranald附近的基建设施规模的光伏（PV）农场的项目',
        '总装机容量为255MWDC，将在一期内完成建造，会是澳大利亚有史以来最大的太阳能光伏农场。'
      ],
      gallery: [
        '/images/media_assets/projects/Sunraysia/Gallery/gallery-ssf-1.jpg',
      ],
      projectMapUrl: '/images/media_assets/projects/Sunraysia/map-ssf.png',
      websiteUrl: '#',
      mapContent: {
        center: {
          lat: -34.8070837,
          lng: 143.493517
        },
        markerPosition: {
          lat: -34.8070837,
          lng: 143.493517
        },
        zoom: 8
      }
    }
  }
}
