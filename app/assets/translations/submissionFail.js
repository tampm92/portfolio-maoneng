export default {
    en: {
      head: {
        title: 'Something went wrong...',
        meta: [
          { hid: 'description', name: 'description', content: '' },
          { hid: 'robots', name: 'robots', content: 'noindex,follow' },
        ]
      },
      titleWhite: 'Something went wrong...',
      subtitle: 'Please try again'
    },
    zh: {
      head: {
        title: 'Something went wrong...',
        meta: [
          { hid: 'description', name: 'description', content: '' },
          { hid: 'robots', name: 'robots', content: 'noindex,follow' },
        ]
      },
      titleWhite: '出了些问题...',
      subtitle: '请重新再试一遍'
    }
  }
