export default {
  en: {
    head: {
      title: 'Maoneng Group | Management Team',
      meta: [
        { hid: 'description', name: 'description', content: 'The team has led the development, financing and delivery of over 500MW+ industrial and utility scaled solar PV projects' }
      ]
    },
    button: {
      profile: 'VIEW PROFILE',
      careers: 'CAREERS',
      submit: 'Submit',
      morenews: 'MORE NEWS'
    },
    heading: {
      title: {
        first: {
          white: 'We deliver'
        },
        second: 'tomorrow\'s',
        third: 'energy.',
        caps: 'OUR TEAM'
      },
    },
    profiles: [
      {
        name: "Morris Zhou",
        avatar: "../images/media_assets/team/Morris.png",
        title: "GROUP EXECUTIVE CHAIRMAN",
        link: "#",
        intro: [
          "Morris Zhou is the Co-Founder and Executive Chairman of MAONENG Group. He previously established the Australian subsidiary of Zhenfa New Energy Science & Technology Co., Ltd, a solar PV development and EPC company originating from China as the Managing Director for Australian since 2010 to 2016.",
          "Having over 10 years of experience in international business, and fluent in both English and Chinese, he is responsible for the group's overarching growth strategy and bridging the need between Chinese capital and International opportunities.",
          "His enduring success is the demonstration of his ability to channel and assemble resources from around the globe to deploy pinpoint strategies that target the hearts of each market segment."
        ]
      },
      {
        name: "Qiao Nan Han",
        avatar: "../images/media_assets/team/Qiao.png",
        title: "Group Vice President",
        link: "#",
        intro: [
          "Qiao is co-founder and Vice President of MAONENG Group. He previously co-founded the Australian subsidiary of Zhenfa New Energy, a solar PV development and EPC Company originating from China, as the General Manager for operations from 2010 to 2016.",
          "Having had extensive experience in construction, risk and negotiations, he is responsible for the overarching investment strategy, execution and delivery of renewable energy assets for the group.",
          "Qiao holds a Bachelor's degree in Civil Engineering, Masters in Sustainable Processing and Masters of Business Administration from Macquarie Graduate School of Management."
        ]
      },
      {
        name: "Milton Zhou",
        avatar: "../images/media_assets/team/Milton.png",
        title: "Group Vice President",
        link: "#",
        intro: [
          "Milton Zhou is a prolific Entrepreneur born and based in Sydney, Australia. Currently he is a proud Co-Founder of MAONENG Group. Milton was previously a director of the Australian subsidiary of Zhenfa New Energy Science & Technology Co., Ltd, strategically planning and executing the exponential growth from 2010 to 2016.",
          "Throughout the years, Milton has been successfully involved in the ever-evolving global Solar PV sector by bridging China and Australia, with his dedication and not-wavering through countless boom and bust cycles. This was all whilst transforming numerous solar business start-ups, by bringing them from zero to success.",
          "Milton Zhou holds a Bachelor's degree in Commerce - Marketing, and is currently undertaking his MBA at MGSM."
        ]
      },
      {
        name: "Patrick Zhu",
        avatar: "../images/media_assets/team/Patrick.png",
        title: "Vice President for S.E. Asia",
        link: "#",
        intro: [
          "Patrick's solar career began in 2010, when he started as a sales representative at a solar retailer. When this solar retailer merged with Zhenfa in 2012, he worked there for 4 years in various roles; sales consultant, business development manager, regional manager.",
          "Throughout his 4 year career in Zhenfa, Patrick was able to accomplish top sales every year.",
          "He also helped the company setup its logistics, business plans & sales strategies. Patrick holds a Bachelor degree in Accounting from university of Macquarie."
        ]
      },
      {
        name: "Kevin Chen",
        avatar: "../images/media_assets/team/Kevin.png",
        title: "Director, Project Finance and M&A",
        link: "#",
        intro: [
          "Kevin joined MAONENG in 2016 and is responsible for overseeing the group's overarching investment strategy, conducting financial valuation and arranging project finance.",
          "He previously worked as part of the Infrastructure. Utilities and Renewables team at Macquarie Capital having advised on infrastructure transactions totally over S40 billion in enterprise value.",
          "Kevin holds a Bachelor of Commerce/Law degree from the University of New South Wales, majoring in Finance. His work is driven by his belief that we need to accelerate the world's transition towards a sustainable energy future."
        ]
      },
      {
        name: "Michael Tran",
        avatar: "../images/media_assets/team/Michael.png",
        title: "Operation Manager - Planning & Engineering",
        link: "#",
        intro: [
          "Michael joined Maoneng in 2017 with previous experience as a systems engineering manager at Knorr-Bremse Australia, with more than 8 years of experience in systems design, project & resource management, testing and commissioning, operations, FMECA, O&M and RAMS.",
          "Michael also brings with him years of experience navigating and leading teams through changing business environment. He is responsible for the day-to-day operation of Mugga Lane Solar Park, project execution of Sunraysia Solar Farm and Maoneng's development pipeline projects, including resource management of external contractors and advisers.",
          "Michael holds a Bachelor of Engineering Science/Law from the University of Technology, Sydney and majors in mechanical Engineering."
        ]
      },
      {
        name: "Antonia Peart",
        avatar: "../images/media_assets/team/Antonia.png",
        title: "Legal and Commercial Director",
        link: "#",
        intro: [
          "Antonia comes from a legal background, having worked at top tier law firms in the UK, France and Australia.",
          "She has extensive experience working on major infrastructure projects globally with an aggregate value of over $10billion."
        ]
      }
    ],
    join: {
      title: 'Join our team.',
      desc:{
        first: 'We’re always looking for talented people. Let us know what you are looking for and we’ll be in contact if anything comes up.',
        second: 'If you want to learn more about what we do and explore our opportunities, check out our careers page.'
      },
    },
    form: {
      title: 'Get in touch',
      name: 'Full Name *',
      number: 'Contact Number *',
      email: 'Email Address *',
      enquiry: 'Enquiry Reason',
      message: 'Message',
      submit: 'SUBMIT',
      requiredFieldMessage: 'Please field out this field'
    },
    news: {
      title: {
        first: 'NEWS',
        second: 'Latest News.',
        third: 'Keep up with the latest on Maoneng Group.'
      },
    },
  },
  zh: {
    button: {
      profile: '查看资料',
      careers: '职业',
      submit: '提交',
      morenews: '更多新闻'
    },
    heading: {
      title: {
        first: {
          white: '我们的团队 与我们'
        },
        second: '共同创造',
        third: '未来',
        caps: '我们的团队'
      },
    },
    profiles: [
      {
        name: "Morris Zhou",
        avatar: "../images/media_assets/team/Morris.png",
        title: "集团执行总裁",
        link: "#",
        intro: [
          "Morris Zhou是茂能集团的联合创始人以及执行总裁。",
          "他曾成立了振发新能源科技有限公司澳大利亚分公司，振发新能源是一家来自中国的太阳能光电发展的总承包商。",
          "Morris在2010至2016年期间担任振发新能源澳大利亚分公司的常务董事。Morris有十多年的国际商务经验，精通中英双语。目前，他负责集团的总体发展战略，并在中国资本与国际机遇之间搭建桥梁。",
          "他的成绩展示了他在全球资源整合，部署针对不同细分市场核心的战略定位等方面的能力。"
        ]
      },
      {
        name: "Qiao Nan Han",
        avatar: "../images/media_assets/team/Qiao.png",
        title: "集团副总裁",
        link: "#",
        intro: [
          "Qiao是茂能集团的联合创始人以及副总裁。",
          "此前他曾参与创立了振发新能源澳大利亚分公司，振发新能源是一家来自中国的太阳能光电发展的总承包商。",
          "Qiao在2010至2016年期间曾在其中担任业务总经理。Qiao在建筑、风险以及谈判等领域经验丰富，目前负责集团整体投资战略制定、执行以及集团可再生能源资产的交付。",
          "Qiao拥有土木工程学学士学位，可持续发展硕士学位，以及麦考瑞大学工商管理学院的MBA学位。"
        ]
      },
      {
        name: "Milton Zhou",
        avatar: "../images/media_assets/team/Milton.png",
        title: "集团副总裁",
        link: "#",
        intro: [
          "Milton Zhou出生并成长于澳大利亚悉尼，是一名富于创新的实干家。",
          "目前是茂能集团的联合创始人之一。此前，Milton曾是振发新能源科技有限公司澳大利亚分公司的一名主管，负责公司战略规划与执行并引领公司从2010年至2016年期间实现了指数级的增长。",
          "在这些年里，Milton通过在中国与澳大利亚之间牵线搭桥，成功参与了不断发展的全球太阳能光电领域，并通过自己的努力与永不言弃的精神，历经了无数次起起落落，在此期间创立了数家太阳能企业，并将它们从零起步，最终走向成功。",
          "Milton Zhou拥有市场营销专业学士学位，目前正在麦考瑞大学工商管理学院攻读MBA。"
        ]
      },
      {
        name: "Patrick Zhu",
        avatar: "../images/media_assets/team/Patrick.png",
        title: "东南亚副总裁",
        link: "#",
        intro: [
          "Patrick早在2010年就开始进入太阳能能源领域，最初在一家太阳能能源销售商担任销售代表。",
          "当该销售商于2012年与振发合并之后至2016年的四年间，他在振发担任了多个职位：销售顾问、业务发展经理、区域经理。",
          "在振发的四年里，Patrick每年都是销售精英。",
          "此外，他还协助公司建立了物流、商业计划&销售战略发展。Patrick毕业于麦格理大学，拥有会计学学士学位。"
        ]
      },
      {
        name: "Kevin Chen",
        avatar: "../images/media_assets/team/Kevin.png",
        title: "项目融资总监",
        link: "#",
        intro: [
          "Kevin于2016年加入茂能集团，主负责集团的总体投资策略，财务估值以及项目融资。",
          "他曾在麦格理资本负责基础建设，公用事业和再生能源。目前已有超过400亿美元交易价值的基础建设的经验。",
          "Kevin拥有新南威尔士大学商学/法学学士学位，主修金融。 他的工作信念：我们需要加速世界向可持续能源未来的过渡。"
        ]
      },
      {
        name: "Michael Tran",
        avatar: "../images/media_assets/team/Michael.png",
        title: "运营经理 - 规划与工程",
        link: "#",
        intro: [
          "Michael于2017年加入茂能，此前曾在Knorr-Bremse Australia担任系统工程经理，在系统设计、项目和资源管理，测试和调试、运营，故障模式、影响及危害性分析，运行和维修, 操作与保养，可靠性、可用性、可维护性以及安全性方面拥有超过8年的经验。",
          "Michael还带着他多年的经验，通过不断变化的商业环境导航和领导团队。",
          "他负责红桉光伏公园的日常运营，Sunraysia 太阳能农场的项目执行和茂能的开发管道项目，包括外部承包商和顾问的资源管理。",
          "Michael拥有悉尼科技大学工程科学/法学学士学位和机械工程专业学士学位。"
        ]
      },
      {
        name: "Antonia Peart",
        avatar: "../images/media_assets/team/Antonia.png",
        title: "法律商务总监",
        link: "#",
        intro: [
          "拥有法律背景的Antonia，曾在英国，法国和澳大利亚的全球顶级跨国律师事务所工作。",
          "从业至今，曾参与总价值100亿+澳币的基建设施项目。",
          "自2015年Antonia开始着手区块链以及加密货币的业务。"
        ]
      }
    ],
    join: {
      title: '加入我们',
      desc:{
        first: '我们一直在寻找优秀的人才。 让我们知道您在寻找什么，如果有任何问题我们会与您联系。',
        second: '如果您想进一步了解我们的公司文化并探索更多机遇，请查询职业页面。'
      },
    },
    news: {
      title: {
        first: '新闻',
        second: '最新新闻',
        third: '了解最新消息'
      }
    }
  }
}
