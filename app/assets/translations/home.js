export default {
  en: {
    head: {
      title: 'Maoneng Group | Developer of Renewable Power Generation Assets',
      meta: [
        { hid: 'description', name: 'description', content: 'Leading the transition from Fossil fuel to renewable energy' }
      ]
    },
    button: {
      moreinfo: 'MORE INFO',
      morenews: 'MORE NEWS',
      team: 'MEET THE TEAM'
    },
    heading: {
      title: {
        first: 'Leading the transition',
        second: 'from fossil fuel to',
        third: 'renewable energy.'
      }
    },
    explore: {
      title: 'Explore',
      projects: 'Operations',
      operation: 'Project locations.',
      muggatitle: 'Mugga Lane Solar Park',
      muggadesc: 'Our first major investment in Australia is the iconic 13MW Mugga Lane Solar Park.',
      muggaUrl: 'https://mlsolarpark.com.au/',
      sunraysiatitle: 'Sunraysia Solar Farm',
      sunraysiadesc: 'Scheduled for completion in December 2019, Sunrasya will have a capacity of 255MWDC and cover over 10km2.',
      sunraysiaUrl: 'http://sunraysiasolarfarm.com.au/',
      projectsMobile: [
        {
          name: 'Mugga Lane Solar Park',
          description: 'Our first major investment in Australia is the iconic 13MW Mugga Lane Solar Park.',
          url: 'https://mlsolarpark.com.au/',
          intro: [
            'Our first major investment in Australia is the iconic 13MW Mugga Lane Solar Park, developed and built under the ACT Government\'s Reverse Solar Auction.',
            'The solar farm generates approximately 24,500 MWh of electricity, which is enough to power more than 3,000 homes. It is built and maintained by UGL - one of the best solar PV construction companies.'
          ]
        },
        {
          name: 'Sunraysia Solar Farm',
          description: 'Largest solar farm in Australia with 255MWDC of capacity, covering over 10km2.',
          url: 'http://sunraysiasolarfarm.com.au/'
        }
      ]
    },
    trackrecord: {
      title: {
        first: 'A',
        second: 'track record',
        third: 'in renewables.'
      },
      description: 'Our management team has led the development, financing and delivery of over 500MW+ industrial and utility scaled rooftop and ground-mounted solar PV projects, involving over $1.5 billion worth of capital investment.',
      card1: {
        title: '500MW+',
        desc: 'industrial and utility scaled rooftop and ground-mounted solar PV projects',
      },
      card2: {
        title: '1.5bn',
        desc: 'worth of capital investment'
      },
    },
    visionteam: {
      menu: {
        vision: 'OUR VISION',
        team: 'OUR TEAM'
      },
      vision: {
        title: {
          first: 'Our vision',
          second: 'is a world where people can co-exist in a sustainble way.'
        },
        desc: {
          first: 'It starts with promoting the awareness of the environment and empowerment of community for the development of socially acceptable infrastructure such as renewable energy and sustainable cities.',
          second: 'Through continuous collaboration and the involvement of stakeholders around the world, our investments together with partners are a testament to our enduring obligation and responsibility as citizens of the earth.',
          third: 'Our boundaries of cooperation are not limited by appearance, gender or beliefs, but rather by the will and passion of all our partners - together we believe that we will make positive contributions to society and the world as a whole.',
          fourth: 'Our success has been the result of key partners which range from the most innovative construction companies to financial partners from around the world where our developments have been underpinned by enduring and prestigious clients including: educational institutions, utilities and Government.',
          fifth: 'Moving forward, we hope to provide a positive influence to all our peers so that the desire for innovation and hunger for success continues to flow.'
        },
      },
      team: {
        title: {
          first: 'TEAM',
          second: 'Our',
          third: 'Team.'
        },
        desc: 'We are the leading builder of large-scale solar farms and pioneering solutions to deliver tomorrow’s energy. Our mission is to accelerate the advent of sustainable energy and the transition towards a world where people can co-exist through sustainable means.'
      },
    },
    news: {
      title: {
        first: 'NEWS',
        second: 'Latest News.',
        third: 'Keep up with the latest on Maoneng Group.'
      },
    },
    project: {
      title: {
        first: 'PROJECT'
      },
      projects: [
        {
          name: 'Mugga Lane.',
          url: '/projects/mugga-lane',
          thumbnail: '/images/media_assets/frontpage/6_Projects/project-mlsp-bg.jpg',
          intro: [
            'Our first major investment in Australia is the iconic 13MW Mugga Lane Solar Park, developed and built under the ACT Government\'s Reverse Solar Auction.',
            'The solar farm generates approximately 24,500 MWh of electricity, which is enough to power more than 3,000 homes. It is built and maintained by UGL - one of the best solar PV construction companies.'
          ]
        },
        {
          name: 'Sunraysia.',
          url: '/projects/sunraysia',
          thumbnail: '/images/media_assets/frontpage/6_Projects/project-slp-bg.jpg',
          intro: [
            'Sunraysia Solar Farm is a utility-scale solar photovoltaic (PV) farm near Balranald in southern New South Wales.',
            'Sunraysia will have a total capacity of 255MWDC and is scheduled for completion in December 2019.'
          ]
        }
      ],
    }
  },
  zh: {
    button: {
      moreinfo: '更多信息',
      morenews: '更多新闻',
      team: '公司团队'
    },
    heading: {
      title: {
        first: '引领从化',
        second: '石能源到',
        third: '可再生能源的转变'
      }
    },
    explore: {
      title: '了解更多',
      projects: '项目',
      operation: '运作地点',
      muggatitle: 'Mugga Lane 光伏公园',
      muggadesc: '茂能在澳大利亚的第一个标志性项目，是通过澳大利亚首都直辖区政府太阳能逆向拍卖活动开发的标杆性的13MW Mugga Lane太阳能公园。',
      muggaUrl: 'https://mlsolarpark.com.au/',
      sunraysiatitle: 'Sunraysia 光伏农场',
      sunraysiadesc: 'Sunraysia 光伏农场是茂能集团位于新南威尔士州南部Balranald附近的基建设施规模的光伏（PV）农场的项目',
      sunraysiaUrl: 'http://sunraysiasolarfarm.com.au/',
      projectsMobile: [
        {
          name: 'Mugga Lane 光伏公园',
          description: '茂能在澳大利亚的第一个标志性项目，是通过澳大利亚首都直辖区政府太阳能逆向拍卖活动开发的标杆性的13MW Mugga Lane太阳能公园。',
          url: 'https://mlsolarpark.com.au/',
          intro: [
            '茂能在澳大利亚的第一个标志性项目，是通过澳大利亚首都直辖区政府太阳能逆向拍卖活动开发的标杆性的13MW Mugga Lane太阳能公园。该太阳能电站大约能产出24,500MWh的电力，足够3,000个家庭使用。该项目由UGL公司承建与维护——该公司是澳大利亚境内最好的一家太阳能光电建设公司之一。'
          ]
        },
        {
          name: 'Sunraysia 光伏农场',
          description: 'Sunraysia 光伏农场是茂能集团位于新南威尔士州南部Balranald附近的基建设施规模的光伏（PV）农场的项目',
          url: 'http://sunraysiasolarfarm.com.au/'
        }
      ]
    },
    trackrecord: {
      title: {
        first: ' ',
        second: '可再生能源的',
        third: '行业业绩'
      },
      description: '茂能的管理团队直接参与过500MW +的工业及基建设施规模的地面光伏项目的开发，融资和运维。总涉及投资金额超过15亿美元。',
      card1: {
        title: '500MW+',
        desc: '工业及基建设施规模的地面光伏项目的开发',
      },
      card2: {
        title: '1.5bn',
        // title: '1.5亿美元',
        desc: '总涉及投资金额'
      },
    },
    visionteam: {
      menu: {
        vision: '我们的愿景是',
        team: '团队'
      },
      vision: {
        title: {
          first: '我们的愿景是',
          second: '一个人们可以以可持续方式共存的世界。'
        },
        desc: {
          first: '它首先要提倡环境意识和社区赋权，以发展可再生能源和可持续城市等社会可接受的基础设施。',
          second: '通过持续的合作和世界各地的利益相关者的参与，我们与合作伙伴一起投资证明了我们作为地球公民的持久责任。',
          third: '我们的合作界限不受外观，性别或信仰的限制，而是受到我们所有合作伙伴的意愿和热情的限制 – 让我们一起相信我们将为社会和整个世界做出积极贡献。',
          fourth: '我们的成功来自于：最具创新性的建筑公司和世界各地的金融合作伙伴等重要合作伙伴的成果，在这些合作伙伴中，我们的发展得到了持久和有声望的客户的支持，其中包括：教育机构，公用事业和政府。',
          fifth: '展望未来，我们希望为所有同行提供积极的影响力，以便创新和渴望成功不断涌现。'
        },
      },
      team: {
        title: {
          first: '团队',
          second: '我们的',
          third: '团队'
        },
        desc: '茂能的管理团队直接参与过500MW +的工业及基建设施规模的地面光伏项目的开发，融资和运维。总涉及投资金额超过15亿美元。'
      },
    },
    news: {
      title: {
        first: '新闻',
        second: '最新新闻',
        third: '了解最新消息'
      },
    },
    project: {
      title: {
        first: '项目'
      },
      projects: [
        {
          name: 'Mugga Lane.',
          url: '/projects/mugga-lane',
          thumbnail: '/images/media_assets/frontpage/6_Projects/project-mlsp-bg.jpg',
          intro: [
            '茂能在澳大利亚的第一个标志性项目，是通过澳大利亚首都直辖区政府太阳能逆向拍卖活动开发的标杆性的13MW Mugga Lane太阳能公园。该太阳能电站大约能产出24,500MWh的电力，足够3,000个家庭使用。该项目由UGL公司承建与维护——该公司是澳大利亚境内最好的一家太阳能光电建设公司之'
          ]
        },
        {
          name: 'Sunraysia.',
          url: '/projects/sunraysia',
          thumbnail: '/images/media_assets/frontpage/6_Projects/project-slp-bg.jpg',
          intro: [
            'Sunraysia 光伏农场是茂能集团位于新南威尔士州南部Balranald附近的基建设施规模的光伏（PV）农场的项目：总装机容量为255MWDC，将在一期内完成建造，会是澳大利亚有史以来最大的太阳能光伏农场。'
          ]
        }
      ],
    }
  },
}
