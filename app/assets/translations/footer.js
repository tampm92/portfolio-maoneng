export default {
  en: {
    terms: 'Terms and conditions',
    disclaimers: 'Disclaimers',
    careers: 'Careers',
    media: 'Media'
  },
  zh: {
    terms: 'Terms and conditions',
    disclaimers: 'Disclaimers',
    careers: '职业',
    media: '媒体'
  }
}
