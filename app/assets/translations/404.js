export default {
  en: {
    title: 'Oops!',
    titleWhite: 'Page not found',
    subtitle: `We can find the page you are looking for. Please go back to the home page.`
  },
  es: {
    title: 'Hey!',
    titleWhite: 'Página no encontrada',
    subtitle: `Podemos encontrar la página que estás buscando. Por favor, vuelva a la página de inicio.`
  }
}
