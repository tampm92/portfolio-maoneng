export default {
  en: {
    map: {
      details: {
        first: 'Maoneng Group',
        second: 'Level 4, 5 Talavera Rd Macquarie Park, NSW 2113, Australia',
        third: '+61 2 9199 8599',
        fourth: 'corporate@maoneng.co'
      },
    },
    menu: {
      title: 'Explore',
      home: 'Home',
      team: 'Team',
      news: 'News',
      projects: 'Projects',
      careers: 'Careers'
    },
    form: {
      title: 'Get in touch',
      name: 'Full Name *',
      number: 'Contact Number *',
      email: 'Email Address *',
      enquiry: 'Enquiry Reason',
      message: 'Message',
      submit: 'SUBMIT',
      requiredFieldMessage: 'Please field out this field'
    },
  },
  zh: {
      map: {
        details: {
          first: '茂能集团',
          second: 'Level 4, 5 Talavera Rd Macquarie Park, 新南威尔士州 2113, 澳大利亚',
          third: '+61 2 9199 8599',
          fourth: 'corporate@maoneng.co'
        },
      },
      menu: {
        title: '了解更多',
        home: '首页',
        team: '团队',
        news: '新闻与媒体',
        projects: '项目',
        careers: 'Careers'
      },
      form: {
        title: '联系我们',
        name: '名称 *',
        number: '联系好吗 *',
        email: '电子邮箱 *',
        enquiry: '咨询目的',
        message: '留言信息',
        submit: '提交'
      },
    },
  }
