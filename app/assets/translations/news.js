export default {
  en: {
    head: {
      title: 'Maoneng Group | News',
      meta: [
        { hid: 'description', name: 'description', content: 'Latest news about the Maoneng Group' }
      ]
    },
    heading: {
      title: {
        first: {
          blue: 'News.',
          white: 'Latest'
        }
      }
    }
  },
  zh: {

  }
}
