export default {
    en: {
      head: {
        title: 'Thank you for your enquiry!',
        meta: [
          { hid: 'description', name: 'description', content: '' },
          { hid: 'robots', name: 'robots', content: 'noindex,follow' },
        ]
      },
      titleWhite: 'Thank you for your enquiry!',
      subtitle: 'We will get back to you shortly'
    },
    zh: {
      head: {
        title: 'Thank you for your enquiry!',
        meta: [
          { hid: 'description', name: 'description', content: '' },
          { hid: 'robots', name: 'robots', content: 'noindex,follow' },
        ]
      },
      titleWhite: '感谢您的咨询!',
      subtitle: '我们会尽快回复您'
    }
  }
