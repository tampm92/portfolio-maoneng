# portfolio-urban-sketch

- After build app, we will have folder `dist`
- Copy content from `app\dist` to `dist`

## Use below script to run docker

```bash
docker-compose up
docker-compose down
docker-compose up --build -d
```